import{ Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {INumber} from 'src/app/number';

@Injectable({
    providedIn:'root'
})

export class DataValueService{
    private baseUrl = 'api/value.json';

    numberValue: INumber;
    // numberChanged =new Subject<number>();

    constructor(private http: HttpClient){ }

    getNumberValue(id: number):Observable<INumber>{
        const url = `${this.baseUrl}/${id}`;
        return this.http.get<INumber>(url).pipe(
            tap(data=>console.log('getNumber' + JSON.stringify(data))),
            catchError(this.handleError)
        );
    }
    
    updateNumber(numb:INumber):Observable<INumber>{
        const headers = new HttpHeaders({'Content-Type':'application/json'});
        const url= `${this.baseUrl}/${1}`;
        return this.http.put<INumber>(url,numb,{headers:headers}).pipe(
            tap(()=>console.log("update number"),
            map(()=>numb))
        );
    }

    private handleError(err:HttpErrorResponse){
        let errorMessage='';
        if(err.error instanceof ErrorEvent){
            errorMessage = `Clientside Error: ${err.error.message}`;
        }
        else{
            errorMessage = `Server error message: ${err.status}, ${err.message}`;
        }

        return throwError(errorMessage);
    }

}