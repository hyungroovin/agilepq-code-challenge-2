import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { DataValueService} from 'src/app/services/app.data.service';
import {Observable} from 'rxjs';
import {INumber} from 'src/app/number'

@Component({
  selector: 'app-number-slider',
  templateUrl: './number-slider.component.html',
  styleUrls: ['./number-slider.component.css']
})
export class NumberSliderComponent implements OnInit {
  sliderForm:FormGroup;
  sliderValue:INumber;
  
  constructor(private fb:FormBuilder, 
              private dataService:DataValueService) {}

  ngOnInit():void{
    this.sliderForm = this.fb.group({
      sliderValue:[null]
    });

    //subscribe to changes
    const ValueControl = this.sliderForm.get('sliderValue');
    ValueControl.valueChanges.subscribe(
      sliderValue=>{
//        this.setFormValue(ValueControl.value);
        this.dataService.updateNumber(ValueControl.value);
      })
  }
  
  // setFormValue(v:number):void{
  //   if(this.sliderValue!=v){
  //     this.sliderValue=v;
  //     this.sliderForm.setValue({
  //     sliderValue:v
  //   })
  //   }
  // }
     
}
