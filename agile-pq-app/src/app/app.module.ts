import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NumberData } from './number-data';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NumberBoxComponent } from './number-box/number-box.component';
import { NumberSliderComponent } from './number-slider/number-slider.component';
import { DataValueService } from './services/app.data.service';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

@NgModule({
  declarations: [
    AppComponent,
    NumberBoxComponent,
    NumberSliderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(NumberData)
  ],
  providers: [DataValueService],
  bootstrap: [AppComponent]
})
export class AppModule { }
