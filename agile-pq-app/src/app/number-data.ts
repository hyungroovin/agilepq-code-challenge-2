import { InMemoryDbService } from 'angular-in-memory-web-api';
import { INumber } from './number';

export class NumberData implements InMemoryDbService {
    createDb() {
        const value:INumber =
        {
            id: 1,
            value:51
        };

        return{value};
    }
    
}