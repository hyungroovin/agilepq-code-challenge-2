import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { DataValueService} from 'src/app/services/app.data.service';
import {Observable} from 'rxjs';
import {INumber} from 'src/app/number';

@Component({
  selector: 'app-number-box',
  templateUrl: './number-box.component.html',
  styleUrls: ['./number-box.component.css']
})
export class NumberBoxComponent implements OnInit {
  numberBoxForm: FormGroup;
  numberBoxValue: INumber;
  valueMessage:string;

  private validationMessages={
    min:'Please Enter a number greater than or equal to zero',
    max:'Please Enter a number less than or equal to one hundred'
  }

  constructor(private fb: FormBuilder,
              private dataService: DataValueService) { }

  ngOnInit():void {
    this.numberBoxForm=this.fb.group({
      numberBoxValue:[null, [Validators.min(0), Validators.max(100)]]
    })

    const valueControl = this.numberBoxForm.get('numberBoxValue');
    valueControl.valueChanges.subscribe(
      numberBoxValue=>{
        // this.setFormValue(valueControl.value);
        this.setMessage(valueControl);
        this.dataService.updateNumber(valueControl.value);
        
      }
    )
  }

  //helper functions

  // setValue():void{
  //   this.dataService.setNumberValue(this.numberBoxValue);
  // }

  // setFormValue(v:number):void{
  //   if(this.numberBoxValue!=v){
  //     this.numberBoxValue=v;
  //     this.numberBoxForm.setValue({
  //     numberBoxValue:v
  //   })
  //   }
  // }

  setMessage(c:AbstractControl):void{
    this.valueMessage='';
    if((c.touched || c.dirty) && c.errors){
      this.valueMessage = Object.keys(c.errors).map(
        key=>this.valueMessage = this.validationMessages[key]).join(' ');
    }
  }

}
